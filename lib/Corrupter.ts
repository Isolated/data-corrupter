/**
 * 	Corrupter - A Node.js library for corrupting data returned by a data source.
 * 
 * 	@author		Michael Palmer
 * 	@version	0.0.1
 * 	@package	data-corrupter
 */

export class Corrupter {
	/**
	 * Converts a snake_case variable name to a pascalCase variable name.
	 * @param {String} variableName The snake_case variable name.
	 * @returns {String} Converted variable name.
	 */
	public static toPascalCase(variableName: string) {
		if (typeof variableName !== 'string') 
			throw new Error('Expected \`variableName\` to be of type: string');
	
		if (RegExp(/[a-z A-Z0-9\\_\\"]+$/).test(variableName)) {
			const varName = variableName.split('_');

			varName[0] = varName[0].charAt(0).toLowerCase() + varName[0].slice(1);

			for (let i = 1; i < varName.length; i++) 
				varName[i] = varName[i].charAt(0).toUpperCase() + varName[i].slice(1);
			
			return varName.join('');
		}

		return variableName;
	}

	/**
	 * Converts an object of snake_case variable names to pascalCase saving the values.
	 * @param {Object} object An object containing snake_case variables.
	 * @returns {Object} An object containing pascalCase variables and original values.
	 */
	public static fromObject(object: object) {
		return new Promise((resolve, reject) => {
			if (typeof object !== 'object')
				return reject(new Error('Expected \`object\` to be of type: Object'));
			
			const result = {};
			for (const oldKey in object) {
				const key = Corrupter.toPascalCase(oldKey);
				result[key] = object[oldKey];
			}
			return resolve(result);
		});
	}
}