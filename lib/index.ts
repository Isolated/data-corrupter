/**
 * 	Corrupter - A Node.js library for corrupting data returned by a database.
 * 
 * 	@author		Michael Palmer
 * 	@version	0.0.1
 * 	@package	data-corrupter
 */

import { Corrupter } from './Corrupter';

export {
	Corrupter
};